package za.co.velvetant.taxi.engine.model.driver;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import za.co.velvetant.taxi.engine.models.DriverStatus;
import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.engine.models.TaxiStatus;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;

public class DriverShiftTestCase {

    private DriverShift shift;
    private Driver driver;
    private Taxi taxi;

    @Before
    public void setUp() throws Exception {

        driver = new Driver(null, null, null, null, null, null, null);
        driver.setActive(true);
        taxi = new Taxi(null, null, null, null, null, null);
        taxi.setActive(true);
        shift = new DriverShift(driver, taxi);
    }

    @Test
    public void newly_created_shift_should_be_() throws Exception {

        assertThat(shift.isAvailable()).isTrue();
    }

    @Test
    public void newly_created_shift_should_online_driver() throws Exception {

        assertThat(driver.getStatus()).isEqualTo(DriverStatus.ONLINE);
    }

    @Test
    public void newly_created_shift_should_online_taxi() throws Exception {

        assertThat(taxi.getStatus()).isEqualTo(TaxiStatus.ONLINE);
    }

    @Test
    public void shift_state_transition_to_busy_should_change_driver_state() throws Exception {

        ifShiftIsAvailable();

        shift.busy();

        assertThat(driver.getStatus()).isEqualTo(DriverStatus.BUSY);
    }

    @Test
    public void shift_state_transition_to_busy_should_change_taxi_state() throws Exception {

        ifShiftIsAvailable();

        shift.busy();

        assertThat(taxi.getStatus()).isEqualTo(TaxiStatus.BUSY);
    }

    @Test
    public void shift_state_transition_available_should_change_driver_state() throws Exception {

        ifShiftIsBusy();

        shift.available();

        assertThat(driver.getStatus()).isEqualTo(DriverStatus.ONLINE);
    }

    @Test
    public void shift_state_transition_to_available_should_change_taxi_state() throws Exception {

        ifShiftIsAvailable();

        shift.available();

        assertThat(taxi.getStatus()).isEqualTo(TaxiStatus.ONLINE);
    }

    @Test
    public void shift_state_transition_to_ended_should_change_driver_state() throws Exception {

        ifShiftIsAvailable();

        shift.endShift();

        assertThat(driver.getStatus()).isEqualTo(DriverStatus.OFFLINE);
    }

    @Test
    public void shift_state_transition_to_ended_should_change_taxi_state() throws Exception {

        ifShiftIsAvailable();

        shift.endShift();

        assertThat(taxi.getStatus()).isEqualTo(TaxiStatus.OFFLINE);
    }

    @Test
    public void ended_shift_cannot_transition_to_available() throws Exception {

        ifShiftHasEnded();

        shift.available();

        assertThat(shift.hasEnded()).isTrue();
    }

    @Test
    public void ended_shift_cannot_transition_to_busy() throws Exception {

        ifShiftHasEnded();

        shift.busy();

        assertThat(shift.hasEnded()).isTrue();
    }

    private void ifShiftHasEnded() {

        shift.endShift();
        driver.setStatus(DriverStatus.OFFLINE);
        taxi.setStatus(TaxiStatus.OFFLINE);
    }

    private void ifShiftIsBusy() {

        shift.busy();
        driver.setStatus(DriverStatus.OFFLINE);
        taxi.setStatus(TaxiStatus.OFFLINE);
    }

    private void ifShiftIsAvailable() {

        shift.available();
        driver.setStatus(DriverStatus.ONLINE);
        taxi.setStatus(TaxiStatus.ONLINE);
    }
}
