package za.co.velvetant.taxi.engine.models;

public interface EntityModel {

    Long getId();

}
