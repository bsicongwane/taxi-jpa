package za.co.velvetant.taxi.engine.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Location implements Serializable {

    @Column(precision = 6, scale = 10)
    private Double longitude;

    @Column(precision = 6, scale = 10)
    private Double latitude;

    private String address;

    public Location() {
    }

    public Location(final Double latitude, final Double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Location(final Double latitude, final Double longitude, final String address) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "dispatch: {" + "longitude=" + longitude + ", latitude=" + latitude + ", address=" + address + '}';
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Location)) {
            return false;
        }

        Location otherLocation = (Location) obj;
        return new EqualsBuilder().append(longitude, otherLocation.longitude).append(latitude, otherLocation.latitude).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(longitude).append(latitude).toHashCode();
    }
}
