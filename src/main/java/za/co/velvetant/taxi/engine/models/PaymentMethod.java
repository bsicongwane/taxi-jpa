package za.co.velvetant.taxi.engine.models;

public enum PaymentMethod {

    CASH, CREDIT_CARD, CORPORATE_ACCOUNT, FAMILY_ACCOUNT;
}
