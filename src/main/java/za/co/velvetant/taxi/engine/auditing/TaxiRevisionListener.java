package za.co.velvetant.taxi.engine.auditing;

import static com.google.common.base.Optional.fromNullable;

import java.security.Principal;

import org.hibernate.envers.RevisionListener;

import za.co.velvetant.taxi.engine.models.BaseEntityModel;

public class TaxiRevisionListener implements RevisionListener {

    public void newRevision(Object revisionEntity) {
        TaxiRevisionEntity taxiRevisionEntity = (TaxiRevisionEntity) revisionEntity;

        Principal principal = fromNullable(BaseEntityModel.getAuthenticatedUserAndClear()).or(new Principal() {

            @Override
            public String getName() {
                return "Unknown";
            }
        });

        taxiRevisionEntity.setUserId(principal.getName());
    }
}
