package za.co.velvetant.taxi.engine.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class RewardGroup extends BaseEntityModel {

    private Long amount;
    @Column(nullable = false, unique = true)
    private String groupName;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(final Long amount) {
        this.amount = amount;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

}
