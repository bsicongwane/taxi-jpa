package za.co.velvetant.taxi.engine.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class FareReference extends BaseEntityModel {

    @OneToOne
    private Fare fare;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    public FareReference() {
    }

    public FareReference(final Long id) {
        setId(id);
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

}
