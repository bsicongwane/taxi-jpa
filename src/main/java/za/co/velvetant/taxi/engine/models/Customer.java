package za.co.velvetant.taxi.engine.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;

@Entity
@Audited
public class Customer extends Person {

    private Boolean sendNewsLetter;

    @OneToMany(mappedBy = "customer")
    private List<Fare> fares;

    @OneToMany(mappedBy = "customer")
    private List<Favourite> favourites;

    @ManyToOne(fetch = FetchType.EAGER)
    private Corporate corporate;

    @OneToOne(mappedBy = "customer", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private RewardPoint rewardPoint;

    @ManyToOne(fetch = FetchType.EAGER)
    private Family family;

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    private List<CreditCard> creditCards = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private Company administered;

    @ManyToOne(fetch = FetchType.EAGER)
    private Family headed;

    @OneToMany(mappedBy = "customer")
    private List<Message> messages;

    @OneToMany(mappedBy = "customer")
    private List<CustomerCoupon> customerCoupons;

    private String pin;

    private String hash;

    public Customer() {
    }

    public Customer(final String firstName, final String lastName, final String cellphone, final String email, final String password) {
        super(firstName, lastName, cellphone, email, email, password);
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(final List<Fare> fares) {
        this.fares = fares;
    }

    public Corporate getCorporate() {
        return corporate;
    }

    public void setCorporate(final Corporate corporate) {
        this.corporate = corporate;
    }

    public List<Favourite> getFavourites() {
        return favourites;
    }

    public void setFavourites(final List<Favourite> favourites) {
        this.favourites = favourites;
    }

    public Boolean getSendNewsLetter() {
        return sendNewsLetter;
    }

    public void setSendNewsLetter(final Boolean sendNewsLetter) {
        this.sendNewsLetter = sendNewsLetter;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(final Family family) {
        this.family = family;
    }

    public RewardPoint getRewardPoint() {
        return rewardPoint;
    }

    public void setRewardPoint(final RewardPoint rewardPoint) {
        this.rewardPoint = rewardPoint;
    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(final List<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }

    public void addCreditCard(final CreditCard creditCard) {
        creditCards.add(creditCard);
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(final List<Message> messages) {
        this.messages = messages;
    }

    public Company getAdministered() {
        return administered;
    }

    public void setAdministered(final Company administered) {
        this.administered = administered;
    }

    public Family getHeaded() {
        return headed;
    }

    public void setHeaded(final Family headed) {
        this.headed = headed;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(final String pin) {
        this.pin = pin;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public List<CustomerCoupon> getCustomerCoupons() {
        return customerCoupons;
    }

    public void setCustomerCoupons(List<CustomerCoupon> customerCoupons) {
        this.customerCoupons = customerCoupons;
    }
}
