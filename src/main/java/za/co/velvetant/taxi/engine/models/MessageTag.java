package za.co.velvetant.taxi.engine.models;

public enum MessageTag {
    INVOICE, PASSWORD_RESET, NEW_ACCOUNT, PROFILE_UPDATE, SERVICE_NOT_AVAILABLE, NO_CABS_AVAILABLE
}
