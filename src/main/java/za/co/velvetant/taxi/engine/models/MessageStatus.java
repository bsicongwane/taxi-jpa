package za.co.velvetant.taxi.engine.models;

public enum MessageStatus {
    SENT, READ
}
