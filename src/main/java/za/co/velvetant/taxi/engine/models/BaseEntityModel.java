package za.co.velvetant.taxi.engine.models;

import java.io.Serializable;
import java.security.Principal;

import javax.persistence.Cacheable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@MappedSuperclass
@Cacheable
public abstract class BaseEntityModel implements EntityModel, Serializable {

    private static ThreadLocal<Principal> authenticatedUser = new ThreadLocal<>();

    public static Principal getAuthenticatedUser() {
        return authenticatedUser.get();
    }

    public static Principal getAuthenticatedUserAndClear() {
        Principal principal = authenticatedUser.get();
        authenticatedUser.remove();

        return principal;
    }

    public static void setAuthenticatedUser(final Principal principal) {
        authenticatedUser.set(principal);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof EntityModel)) {
            return false;
        }

        return new EqualsBuilder().append(id, ((EntityModel) obj).getId()).isEquals();
    }

}
