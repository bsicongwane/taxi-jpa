package za.co.velvetant.taxi.engine.models;

import org.hibernate.envers.Audited;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@Audited
public class Sector extends BaseEntityModel {

    private String name;

    @OneToMany(mappedBy = "sector")
    private List<Corporate> corporates;

    public Sector() {
    }

    public Sector(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<Corporate> getCorporates() {
        return corporates;
    }

    public void setCorporates(final List<Corporate> corporates) {
        this.corporates = corporates;
    }
}
