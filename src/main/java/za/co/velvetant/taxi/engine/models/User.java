package za.co.velvetant.taxi.engine.models;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;

@Entity
@Audited
public class User extends Person {

    public User() {
    }

    public User(final String firstName, final String lastName, final String cellphone, final String email, final String password) {
        super(firstName, lastName, cellphone, email, email, password);
    }

}
