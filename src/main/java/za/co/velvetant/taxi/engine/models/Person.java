package za.co.velvetant.taxi.engine.models;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

@Entity
@Audited
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class Person extends BaseEntityModel {

    private Boolean active = true;

    @Column(nullable = false)
    @Index(name = "idx_email")
    private String email;

    private String firstName;

    private String lastName;

    @Column(nullable = false)
    @Index(name = "idx_cellphone")
    private String cellphone;

    private String password;

    @Column(unique = true, nullable = false)
    @Index(name = "idx_userid")
    private String userId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();

    @OneToMany(mappedBy = "person", cascade = {CascadeType.ALL})
    private List<Role> groups = new ArrayList<>();

    @Lob
    @Basic(fetch = FetchType.EAGER)
    private byte[] photo;
    private String photoFormat;

    public Person() {
    }

    public Person(final String firstName, final String lastName, final String cellphone, final String email, final String userId, final String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cellphone = cellphone;
        this.email = email;
        this.userId = userId;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(final String cellphone) {
        this.cellphone = cellphone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public List<Role> getGroups() {
        return groups;
    }

    public void setGroups(final List<Role> groups) {
        this.groups = groups;
    }

    public void addGroup(final Role role) {
        groups.add(role);
    }

    public String getEmail() {
        return email;
    }

    public Date getCreatedDate() {
        Date date = createdDate;
        if (createdDate != null) {
            date = new Date(createdDate.getTime());
        }

        return date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(final byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoFormat() {
        return photoFormat;
    }

    public void setPhotoFormat(final String photoFormat) {
        this.photoFormat = photoFormat;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    public void setCreatedDate(final Date createdDate) {
        this.createdDate = createdDate;
    }

    public String fullName() {
        return format("%s %s", firstName, lastName);
    }
}
