package za.co.velvetant.taxi.engine.models;

public enum RatingLevel {

    BAD(1), SO_SO(2), OK(3), GOOD(4), EXCELLENT(5);

    private int rating;

    private RatingLevel(final int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public static RatingLevel getRating(final int rating) {
        for (RatingLevel c : RatingLevel.values()) {
            if (c.getRating() == rating) {
                return c;
            }
        }
        return null;
    }
}
