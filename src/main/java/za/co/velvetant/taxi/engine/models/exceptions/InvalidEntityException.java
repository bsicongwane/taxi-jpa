package za.co.velvetant.taxi.engine.models.exceptions;

public class InvalidEntityException extends RuntimeException {

    public InvalidEntityException(final String message) {
        super(message);
    }
}
