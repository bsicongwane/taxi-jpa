package za.co.velvetant.taxi.engine.models;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Entity
@Audited
public class Role extends BaseEntityModel {

    @Enumerated(EnumType.STRING)
    private Group role;

    @ManyToOne
    private Person person;

    public Role() {
    }

    public Role(final Group role, final Person person) {
        this.role = role;
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(final Person person) {
        this.person = person;
    }

    public Group getRole() {
        return role;
    }

    public void setRole(final Group role) {
        this.role = role;
    }

}
