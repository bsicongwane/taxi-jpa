package za.co.velvetant.taxi.engine.auditing;

import javax.persistence.Entity;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

@Entity
@RevisionEntity(TaxiRevisionListener.class)
public class TaxiRevisionEntity extends DefaultRevisionEntity {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }
}
