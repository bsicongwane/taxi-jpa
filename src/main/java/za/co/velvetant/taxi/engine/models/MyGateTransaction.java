package za.co.velvetant.taxi.engine.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class MyGateTransaction extends BaseEntityModel {

    public enum Type {
        STORE_CARD, UPDATE_CARD, REMOVE_CARD, PAY
    }

    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionTime;

    @OneToOne
    private Fare fare;

    @OneToOne
    private Customer customer;

    @OneToOne
    private Family family;

    @OneToOne
    private Corporate corporate;

    @Enumerated(EnumType.STRING)
    private Type transactionType;

    private String transactionIndex;
    private Integer status = -1;

    private String errorMessage;
    private String authorisationId;

    public MyGateTransaction() {
    }

    public MyGateTransaction(final Long id) {
        setId(id);
    }

    public MyGateTransaction(final Date transactionTime, final Fare fare, final Type transactionType, final String transactionIndex, final Integer status) {
        super();
        this.transactionTime = transactionTime;
        this.fare = fare;
        this.transactionType = transactionType;
        this.transactionIndex = transactionIndex;
        this.status = status;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public Fare getFare() {
        return fare;
    }

    public Type getTransactionType() {
        return transactionType;
    }

    public String getTransactionIndex() {
        return transactionIndex;
    }

    public Integer getStatus() {
        return status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(final Family family) {
        this.family = family;
    }

    public Corporate getCorporate() {
        return corporate;
    }

    public void setCorporate(final Corporate corporate) {
        this.corporate = corporate;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getAuthorisationId() {
        return authorisationId;
    }

    public void setAuthorisationId(final String authorisationId) {
        this.authorisationId = authorisationId;
    }

    public boolean isSuccessful() {
        return status == 0;
    }

}
