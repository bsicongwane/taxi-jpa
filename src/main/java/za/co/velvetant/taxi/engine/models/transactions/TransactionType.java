package za.co.velvetant.taxi.engine.models.transactions;

public enum TransactionType {

    CASH, CREDIT_CARD, COUPON;
}
