package za.co.velvetant.taxi.engine.models.coupons;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

import za.co.velvetant.taxi.engine.models.BaseEntityModel;
import za.co.velvetant.taxi.engine.models.Customer;

@Entity
@Audited
public class CustomerCoupon extends BaseEntityModel {

    private String uuid = UUID.randomUUID().toString();
    private Boolean active = true;

    @Temporal(TIMESTAMP)
    private Date redemptionDate;

    @ManyToOne
    private Coupon coupon;
    
    @ManyToOne
    private Customer customer;

    public CustomerCoupon() {
    }

    public CustomerCoupon(final Coupon coupon, final Customer customer) {
        this.coupon = coupon;
        this.customer = customer;
    }

    public CustomerCoupon redeem() {
        redemptionDate = new Date();

        return this;
    }

    public CustomerCoupon expire() {
        active = false;

        return this;
    }

    public boolean isRedeemed() {
        return redemptionDate != null;
    }

    public boolean hasExpired() {
        return !active;
    }

    public String getUuid() {
        return uuid;
    }

    public Customer getCustomer() {

        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(final Coupon coupon) {
        this.coupon = coupon;
    }

    public Date getRedemptionDate() {
        return redemptionDate;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(uuid).hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if(obj instanceof CustomerCoupon) {
            equals = new EqualsBuilder().append(uuid, ((CustomerCoupon) obj).getUuid()).isEquals();
        }

        return equals;
    }
}
