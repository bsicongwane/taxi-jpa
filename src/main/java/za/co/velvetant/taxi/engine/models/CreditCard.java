package za.co.velvetant.taxi.engine.models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class CreditCard extends BaseEntityModel {

    private String gateWayId;
    
    private String cvvReference;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Family family;

    @ManyToOne
    private Corporate corporate;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public String getGateWayId() {
        return gateWayId;
    }

    public void setGateWayId(final String gateWayId) {
        this.gateWayId = gateWayId;
    }

    public Family getFamily() {
        return family;
    }

    public Corporate getCorporate() {
        return corporate;
    }

    public void setFamily(final Family family) {
        this.family = family;
    }

    public void setCorporate(final Corporate corporate) {
        this.corporate = corporate;
    }

	public String getCvvReference() {
		return cvvReference;
	}

	public void setCvvReference(String cvvReference) {
		this.cvvReference = cvvReference;
	}
    
    

}
