package za.co.velvetant.taxi.engine.models;

import org.hibernate.envers.Audited;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Audited
public class Favourite extends BaseEntityModel {

    private String name;

    @ManyToOne
    private Customer customer;

    @Embedded
    private Location location;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(final Location location) {
        this.location = location;
    }

}
