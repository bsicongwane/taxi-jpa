package za.co.velvetant.taxi.engine.models;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

@Entity
@Audited
@NamedQueries({ @NamedQuery(name = Taxi.Queries.FIND_BY_REGISTRATION, query = "SELECT t FROM Taxi t WHERE t.registration = ?1") })
public class Taxi extends BaseEntityModel {

    public static final class Queries {

        public static final String FIND_BY_REGISTRATION = "Taxi.findByRegistration";
    }

    private String taxiId;

    @Column(unique = true, nullable = false)
    @Index(name = "idx_registration")
    private String registration;

    private String make;
    private String model;
    private String color;
    private Integer capacity;
    private Boolean active;

    @Enumerated(EnumType.STRING)
    private TaxiStatus status;

    @Embedded
    private DriverBox driverBox;

    @Embedded
    private Location lastLocation;

    public Taxi() {
    }

    public Taxi(final Long id) {
        setId(id);
    }

    public Taxi(final String taxiId, final String registration, final String make, final String model, final String color, final Integer capacity) {

        this.taxiId = taxiId;
        this.registration = registration;
        this.make = make;
        this.model = model;
        this.color = color;
        this.capacity = capacity;
        this.status = TaxiStatus.OFFLINE;
        this.active = true;
    }

    public boolean isOnline() {

        return TaxiStatus.ONLINE.equals(status);
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getRegistration() {
        return registration;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public TaxiStatus getStatus() {
        return status;
    }

    public void setStatus(final TaxiStatus status) {
        this.status = status;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    public DriverBox getDriverBox() {
        return driverBox;
    }

    public void setDriverBox(final DriverBox driverBox) {
        this.driverBox = driverBox;
    }

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(final Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(registration).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {

        if (!(obj instanceof Taxi)) {
            return false;
        }

        return new EqualsBuilder().append(registration, ((Taxi) obj).getRegistration()).isEquals();
    }
}
