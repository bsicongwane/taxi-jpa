package za.co.velvetant.taxi.engine.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class RewardPoint extends BaseEntityModel {

    private Long total;
    private String rewardGroup;

    @OneToOne
    private Customer customer;

    @OneToMany(mappedBy = "rewardPoint", cascade = { CascadeType.ALL })
    private List<RewardPointLog> rewardPointLogs;

    public RewardPoint() {

    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(final Long total) {
        this.total = total;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public List<RewardPointLog> getRewardPointLogs() {
        return rewardPointLogs;
    }

    public void setRewardPointLogs(final List<RewardPointLog> rewardPointLogs) {
        this.rewardPointLogs = rewardPointLogs;
    }

    public void earn(final Long points, final Fare fare) {

        if (rewardPointLogs == null) {
            rewardPointLogs = new ArrayList<>();
            total = 0L;
        }
        final RewardPointLog rewardPointLog = new RewardPointLog(this, points, new Date(System.currentTimeMillis()), TransactionType.CREDIT, fare);
        rewardPointLogs.add(rewardPointLog);
        total += points;
    }

    public void redeem(final Long points, final Fare fare) {

        if (rewardPointLogs == null) {
            rewardPointLogs = new ArrayList<>();
            total = 0L;
        }
        final RewardPointLog rewardPointLog = new RewardPointLog(this, points, new Date(System.currentTimeMillis()), TransactionType.DEBIT, fare);
        rewardPointLogs.add(rewardPointLog);
        total -= points;
    }

    public boolean eligible() {
        // TODO is there a minimum?
        return total != null && total > 0;
    }

    public String getRewardGroup() {
        return rewardGroup;
    }

    public void setRewardGroup(final String rewardGroup) {
        this.rewardGroup = rewardGroup;
    }

}
