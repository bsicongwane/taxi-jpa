package za.co.velvetant.taxi.engine.models;

import javax.persistence.Embeddable;

@Embeddable
public class DriverBox {

    private String serialNumber;
    private String version;

    public DriverBox() {
    }

    public DriverBox(final String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public DriverBox(final String serialNumber, final String version) {
        this(serialNumber);
        this.version = version;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }
}
