package za.co.velvetant.taxi.engine.models;

public enum DriverStatus {
    OFFLINE, ONLINE, BUSY
}
