package za.co.velvetant.taxi.engine.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

@Entity
@Audited
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
public class Company extends BaseEntityModel {

    private String companyName;
    private String billingAddress1;
    private String billingAddress2;
    private String billingAddress3;
    private String billingPostalCode;
    private String postalAddress1;
    private String postalAddress2;
    private String postalAddress3;
    private String postalPostalCode;

    @OneToMany(mappedBy = "administered")
    private List<Customer> administrators = new ArrayList<>();

    @Column(unique = true)
    private String registration;
    private String vat;
    private Boolean taxExempt;

    private Boolean active = true;

    public Company() {
    }

    public Company(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode, final String registration, final String vat, final Boolean taxExempt) {
        this.companyName = companyName;
        this.billingAddress1 = billingAddress1;
        this.billingAddress2 = billingAddress2;
        this.billingAddress3 = billingAddress3;
        this.billingPostalCode = billingPostalCode;
        this.postalAddress1 = postalAddress1;
        this.postalAddress2 = postalAddress2;
        this.postalAddress3 = postalAddress3;
        this.postalPostalCode = postalPostalCode;

        this.registration = registration;
        this.vat = vat;
        this.taxExempt = taxExempt;
    }

    public void addAdministrator(final Customer customer) {
        getAdministrators().add(customer);
    }

    public void removeAdministrator(final Customer customer) {
        getAdministrators().remove(customer);
    }

    public boolean isAdministrator(final String userId) {
        return getAdministrators().contains(userId);
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(final String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(final String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingAddress3() {
        return billingAddress3;
    }

    public void setBillingAddress3(final String billingAddress3) {
        this.billingAddress3 = billingAddress3;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(final String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    public String getPostalAddress1() {
        return postalAddress1;
    }

    public void setPostalAddress1(final String postalAddress1) {
        this.postalAddress1 = postalAddress1;
    }

    public String getPostalAddress2() {
        return postalAddress2;
    }

    public void setPostalAddress2(final String postalAddress2) {
        this.postalAddress2 = postalAddress2;
    }

    public String getPostalAddress3() {
        return postalAddress3;
    }

    public void setPostalAddress3(final String postalAddress3) {
        this.postalAddress3 = postalAddress3;
    }

    public String getPostalPostalCode() {
        return postalPostalCode;
    }

    public void setPostalPostalCode(final String postalPostalCode) {
        this.postalPostalCode = postalPostalCode;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(final String registration) {
        this.registration = registration;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(final String vat) {
        this.vat = vat;
    }

    public Boolean getTaxExempt() {
        return taxExempt;
    }

    public void setTaxExempt(final Boolean taxExempt) {
        this.taxExempt = taxExempt;
    }

    public List<Customer> getAdministrators() {
        return administrators;
    }

    public void setAdministrators(final List<Customer> administrators) {
        this.administrators = administrators;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.getId()).append(companyName).append(registration).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof EntityModel)) {
            return false;
        }

        final Company company = (Company) obj;
        return new EqualsBuilder().append(this.getId(), company.getId()).append(companyName, company.getCompanyName()).append(registration, company.registration).isEquals();
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

}
