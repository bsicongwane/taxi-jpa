package za.co.velvetant.taxi.engine.models;

public enum TransactionType {
    DEBIT, CREDIT
}
