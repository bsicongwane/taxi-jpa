package za.co.velvetant.taxi.engine.models;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

import org.hibernate.envers.Audited;

@Entity
@Audited
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class Audit extends BaseEntityModel {

    @Temporal(TIMESTAMP)
    private Date actionedOn = new Date();
    
    private String description;
    private Long linkedId;

    @ManyToOne
    private Person actionedBy;

    public Audit() {
    }

    public Audit(final Person actionedBy, final String description) {
        this.actionedBy = actionedBy;
        this.description = description;
    }

    public Date getActionedOn() {
        return actionedOn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Long getLinkedId() {
        return linkedId;
    }

    public void setLinkedId(final Long linkedId) {
        this.linkedId = linkedId;
    }

    public Person getActionedBy() {
        return actionedBy;
    }
}
