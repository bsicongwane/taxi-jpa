package za.co.velvetant.taxi.engine.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class RewardPointLog extends BaseEntityModel {

    @ManyToOne
    private RewardPoint rewardPoint;

    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionTime;

    private Long amount;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @OneToOne
    private Fare fare;

    public RewardPointLog() {
    }

    public RewardPointLog(final RewardPoint rewardPoint, final Long amount, final Date transactionTime, final TransactionType transactionType, final Fare fare) {
        super();
        this.rewardPoint = rewardPoint;
        this.transactionTime = transactionTime;
        this.amount = amount;
        this.transactionType = transactionType;
        this.fare = fare;
    }

    public RewardPoint getRewardPoint() {
        return rewardPoint;
    }

    public void setRewardPoint(final RewardPoint rewardPoint) {
        this.rewardPoint = rewardPoint;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(final Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(final Long amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(final TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(final Fare fare) {
        this.fare = fare;
    }
}
