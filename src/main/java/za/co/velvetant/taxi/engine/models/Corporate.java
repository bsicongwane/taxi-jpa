package za.co.velvetant.taxi.engine.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Corporate extends Company {

    @ManyToOne
    private Sector sector;

    @ManyToOne
    private Industry industry;

    @OneToMany(mappedBy = "corporate")
    private List<Customer> customers;

    @OneToMany(mappedBy = "corporate")
    private List<CreditCard> creditCards = new ArrayList<>();

    private String hash;

    public Corporate() {
    }

    public Corporate(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode, final String registration, final String vat, final Boolean taxExempt, final Sector sector,
            final Industry industry) {
        super(companyName, billingAddress1, billingAddress2, billingAddress3, billingPostalCode, postalAddress1, postalAddress2, postalAddress3, postalPostalCode, registration, vat, taxExempt);
        this.sector = sector;
        this.industry = industry;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(final Sector sector) {
        this.sector = sector;
    }

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(final Industry industry) {
        this.industry = industry;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(final List<Customer> customers) {
        this.customers = customers;
    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void addCreditCard(final CreditCard creditCard) {
        creditCards.add(creditCard);
    }

    public String getHash() {
        return hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

}
