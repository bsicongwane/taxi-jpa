package za.co.velvetant.taxi.engine.models;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CASH;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

import za.co.velvetant.taxi.engine.models.driver.Driver;

@Entity
@Audited
@NamedQueries({@NamedQuery(name = "Fare.findByCustomerEmail", query = "SELECT o FROM Fare o WHERE o.customer.email = ?1 GROUP BY o.id, o.requestTime ORDER BY o.requestTime DESC"),
        @NamedQuery(name = "Fare.findByUUID", query = "SELECT o FROM Fare o WHERE o.uuid = ?1 GROUP BY o.id, o.requestTime ORDER BY o.requestTime DESC"),
        @NamedQuery(name = "Fare.CheckIfUuidExists", query = Fare.CHECK_UUID)})
public class Fare extends BaseEntityModel {

    public static final String CHECK_UUID = "SELECT COUNT(o.uuid) FROM Fare o WHERE o.uuid = ?1";

    @Column(unique = true, nullable = false)
    @Index(name = "idx_uuid")
    private String uuid;

    @ManyToOne(fetch = FetchType.EAGER)
    private Customer customer;

    @ManyToOne(optional = true)
    private Driver driver;

    @Enumerated(STRING)
    private PaymentMethod paymentMethod = CASH;

    @OneToOne(mappedBy = "fare", cascade = ALL)
    private Rating rating;

    @Temporal(TemporalType.TIMESTAMP)
    private Date acceptedTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date requestTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dropOffTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date pickUpTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date pickedUpTime;

    private Double tip = 0.0;
    private Double amount = 0.0;
    private Long distance = 0L;
    private Long actualDistance = 0L;
    private Long duration = 0L;
    private Long actualDuration = 0L;
    private String reference;

    @ManyToOne(optional = true)
    private Taxi taxi;

    @OneToMany(cascade = ALL)
    private List<StopLocation> stops = new ArrayList<>();

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "longitude", column = @Column(name = "PICKUP_LONGITUDE")), @AttributeOverride(name = "latitude", column = @Column(name = "PICKUP_LATITUDE")),
            @AttributeOverride(name = "address", column = @Column(name = "PICKUP_ADDRESS"))})
    private Location pickup;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "longitude", column = @Column(name = "DROPOFF_LONGITUDE")), @AttributeOverride(name = "latitude", column = @Column(name = "DROPOFF_LATITUDE")),
            @AttributeOverride(name = "address", column = @Column(name = "DROPOFF_ADDRESS"))})
    private Location dropOff;

    @OneToOne(mappedBy = "fare")
    private RewardPointLog rewardPointLog;

    @Enumerated(STRING)
    @Index(name = "idx_state")
    private FareState state;

    @OneToMany
    private List<Audit> audits = new ArrayList<>();

    public Fare() {
    }

    public Fare(final Customer customer, final Date requestTime, final Date pickupTime, final Location pickup, final Location dropOff, final String uuid) {
        this.customer = customer;
        this.pickup = pickup;
        this.dropOff = dropOff;
        this.uuid = uuid;

        if (requestTime != null) {
            this.requestTime = new Date(requestTime.getTime());
        }

        if (pickupTime != null) {
            this.pickUpTime = new Date(pickupTime.getTime());
        }
    }

    public Fare(final Customer customer, final Date requestTime, final Date pickupTime, final Location pickup, final String uuid) {
        this(customer, requestTime, pickupTime, pickup, null, uuid);
    }

    public void addStop(final StopLocation location) {
        stops.add(location);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(final Driver driver) {
        this.driver = driver;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(final PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(final Rating rating) {
        this.rating = rating;
    }

    public Date getAcceptedTime() {
        Date date = acceptedTime;
        if (acceptedTime != null) {
            date = new Date(acceptedTime.getTime());
        }

        return date;
    }

    public void setAcceptedTime(final Date acceptedTime) {
        if (acceptedTime != null) {
            this.acceptedTime = new Date(acceptedTime.getTime());
        }
    }

    public Date getRequestTime() {
        Date date = requestTime;
        if (requestTime != null) {
            date = new Date(requestTime.getTime());
        }

        return date;
    }

    public Date getDropOffTime() {
        Date date = dropOffTime;
        if (dropOffTime != null) {
            date = new Date(dropOffTime.getTime());
        }

        return date;
    }

    public void setDropOffTime(final Date dropOffTime) {
        if (dropOffTime != null) {
            this.dropOffTime = new Date(dropOffTime.getTime());
        }
    }

    public Date getPickUpTime() {
        Date date = pickUpTime;
        if (pickUpTime != null) {
            date = new Date(pickUpTime.getTime());
        }

        return date;
    }

    public void setPickUpTime(final Date pickUpTime) {
        if (pickUpTime != null) {
            this.pickUpTime = new Date(pickUpTime.getTime());
        }
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(final Double amount) {
        this.amount = amount;
    }

    public Taxi getTaxi() {
        return taxi;
    }

    public void setTaxi(final Taxi taxi) {
        this.taxi = taxi;
    }

    public Location getPickup() {
        return pickup;
    }

    public void setPickup(final Location pickup) {
        this.pickup = pickup;
    }

    public Location getDropOff() {
        return dropOff;
    }

    public void setDropOff(final Location dropOff) {
        this.dropOff = dropOff;
    }

    public Date getPickedUpTime() {
        Date date = pickedUpTime;
        if (pickedUpTime != null) {
            date = new Date(pickedUpTime.getTime());
        }

        return date;
    }

    public void setPickedUpTime(final Date pickedUpTime) {
        if (pickedUpTime != null) {
            this.pickedUpTime = new Date(pickedUpTime.getTime());
        }
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(final Long distance) {
        this.distance = distance;
    }

    public Long getActualDistance() {
        return actualDistance;
    }

    public void setActualDistance(final Long actualDistance) {
        this.actualDistance = actualDistance;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(final Long duration) {
        this.duration = duration;
    }

    public Long getActualDuration() {
        return actualDuration;
    }

    public void setActualDuration(final Long actualDuration) {
        this.actualDuration = actualDuration;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public void setRequestTime(final Date requestTime) {
        if (requestTime != null) {
            this.requestTime = new Date(requestTime.getTime());
        }
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(final Double tip) {
        this.tip = tip;
    }

    public RewardPointLog getRewardPointLog() {
        return rewardPointLog;
    }

    public void setRewardPointLog(final RewardPointLog rewardPointLog) {
        this.rewardPointLog = rewardPointLog;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(final String reference) {
        this.reference = reference;
    }

    public FareState getState() {
        return state;
    }

    public void setState(final FareState state) {
        this.state = state;
    }

    public List<Audit> getAudits() {
        return audits;
    }

    public void addAudit(final Audit audit) {
        audits.add(audit);
    }

    public boolean isOverriddenOn(FareState state) {
        boolean overridden = false;

        for (Audit audit : audits) {
            switch (state) {
                case CAPTURE_FARE_AMOUNT: {
                    if (audit instanceof CaptureTripAmountOverride) {
                        overridden = true;
                        break;
                    }
                }
                    break;
                case AWAIT_PAYMENT_CONFIRMATION: {
                    if (audit instanceof ConfirmPaymentOverride) {
                        overridden = true;
                        break;
                    }
                }
                    break;
            }
        }

        return overridden;
    }
}
