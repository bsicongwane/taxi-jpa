package za.co.velvetant.taxi.engine.models.coupons;

import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import za.co.velvetant.taxi.engine.models.BaseEntityModel;
import za.co.velvetant.taxi.engine.models.exceptions.InvalidEntityException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static javax.persistence.TemporalType.TIMESTAMP;

@Entity
@Audited
public class Coupon extends BaseEntityModel {

    @Column(unique = true, nullable = false)
    private String uuid = UUID.randomUUID().toString();

    private Boolean active = true;
    private String couponNumber;
    private String description;

    @Temporal(TIMESTAMP)
    private Date validFrom;

    @Temporal(TIMESTAMP)
    private Date validTo;

    private BigDecimal monetaryLimit;
    private Integer usageLimit;

    @OneToMany
    private Set<CustomerCoupon> customerCoupons = new HashSet<>();

    public Coupon() {
    }

    public Coupon(final String couponNumber,
                  final String description,
                  final Date validFrom,
                  final Date validTo,
                  final BigDecimal monetaryLimit,
                  final Integer usageLimit) {
        this.couponNumber = couponNumber;
        this.description = description;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.monetaryLimit = monetaryLimit;
        this.usageLimit = usageLimit;
    }

    @PrePersist
    public void validate() {
        if (hasExpired()) {
            throw new InvalidEntityException("Cannot create coupon. The valid to date has already passed.");
        }
    }

    public boolean hasExpired() {
        boolean expired = false;
        if (!active || new DateTime(validTo).isBeforeNow()) {
            expired = true;
        }

        return expired;
    }

    public Coupon expire() {
        active = false;

        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public String getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(final Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(final Date validTo) {
        this.validTo = validTo;
    }

    public Integer getUsageLimit() {
        return usageLimit;
    }

    public void setUsageLimit(final Integer usageLimit) {
        this.usageLimit = usageLimit;
    }

    public BigDecimal getMonetaryLimit() {
        return monetaryLimit;
    }

    public void setMonetaryLimit(final BigDecimal monetaryLimit) {
        this.monetaryLimit = monetaryLimit;
    }

    public Set<CustomerCoupon> getCustomerCoupons() {
        return customerCoupons;
    }

    public void setCustomerCoupons(final Set<CustomerCoupon> customerCoupons) {
        this.customerCoupons = customerCoupons;
    }

    public void addCustomerCoupon(final CustomerCoupon customerCoupon) {
        this.customerCoupons.add(customerCoupon);
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }
}
