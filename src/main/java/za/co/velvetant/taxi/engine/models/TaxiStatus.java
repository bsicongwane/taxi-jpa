package za.co.velvetant.taxi.engine.models;

public enum TaxiStatus {
    OFFLINE, ONLINE, BUSY
}
