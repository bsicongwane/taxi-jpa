package za.co.velvetant.taxi.engine.models;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@NamedNativeQueries({
    // Credits: http://stackoverflow.com/a/18222124
    @NamedNativeQuery(name = "TaxiLocation.findLatest", query =
            "SELECT o.* FROM TaxiLocation o "
            +    "INNER JOIN ("
            +        "SELECT taxi_id, MAX(entryDate) AS entryDate "
            +        "FROM TaxiLocation "
            +        "GROUP BY taxi_id "
            +    ") AS max "
            +    "USING (taxi_id, entryDate) "
            + "GROUP BY o.entryDate", resultClass = TaxiLocation.class),
    @NamedNativeQuery(name = "TaxiLocation.findLatestByTaxi", query =
            "SELECT o.* FROM TaxiLocation o "
            +    "INNER JOIN ("
            +        "SELECT taxi_id, MAX(entryDate) AS entryDate "
            +        "FROM TaxiLocation "
            +        "GROUP BY taxi_id "
            +    ") AS max "
            +    "USING (taxi_id, entryDate) "
            + "WHERE o.taxi_id = ?1 "
            + "GROUP BY o.entryDate", resultClass = TaxiLocation.class),

    @NamedNativeQuery(name = "TaxiLocation.findLatestByRegistration", query =
            "SELECT o.* FROM TaxiLocation o "
            +    "INNER JOIN ("
            +        "SELECT taxi_id, MAX(entryDate) AS entryDate "
            +        "FROM TaxiLocation "
            +        "GROUP BY taxi_id "
            +    ") AS max "
            +    "USING (taxi_id, entryDate) "
            +    "JOIN Taxi AS t "
            +    "ON (o.taxi_id = t.id) "
            + "WHERE t.registration = ?1 "
            + "GROUP BY o.entryDate", resultClass = TaxiLocation.class)
})
public class TaxiLocation extends BaseEntityModel {

    @ManyToOne(optional = false)
    private Taxi taxi;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "idx_entryDate")
    private Date entryDate = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date publishedDate = new Date();

    @Embedded
    private Location location;

    public TaxiLocation() {
    }

    public TaxiLocation(final Taxi taxi, final Location location) {
        this.location = location;
        this.taxi = taxi;
    }

    public TaxiLocation(final Taxi taxi, final Location location, final Date entryDate) {
        this(taxi, location);
        this.entryDate = entryDate;
    }

    public Taxi getTaxi() {
        return taxi;
    }

    public Date getEntryDate() {
        return new Date(entryDate.getTime());
    }

    public void setEntryDate(final Date entryDate) {
        this.entryDate = new Date(entryDate.getTime());
    }

    public Location getLocation() {
        return location;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }
}
