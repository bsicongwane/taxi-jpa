package za.co.velvetant.taxi.engine.models;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class StopLocation {

    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    private Location location;

    @Temporal(TIMESTAMP)
    private Date time = new Date();

    protected StopLocation() {
    }

    public StopLocation(final Double latitude, final Double longitude) {
        location = new Location(latitude, longitude);
    }

    public StopLocation(final Double latitude, final Double longitude, final String address) {
        location = new Location(latitude, longitude, address);
    }

    public Date getTime() {
        return time;
    }

    public void setTime(final Date time) {
        this.time = time;
    }

    public Long getId() {
        return id;
    }

}
