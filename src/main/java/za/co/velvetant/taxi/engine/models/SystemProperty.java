package za.co.velvetant.taxi.engine.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class SystemProperty extends BaseEntityModel {

    public enum Name {

        REWARD_POINTS_PERECENTAGE("rewardpoints.cost.percentage"),
        ELIGIBLE_DRIVER_ACCEPTANCE_DURATION("eligible.driver.acceptance.duration"),
        RATING_COMPLETION_DURATION("rating.completion.duration"),
        ALLOWED_PAYMENT_CONFIRMATIONS("allowed.payment.confirmations"),
        PAYMENT_CONFIRMATION_DURATION("payment.confirmation.duration"),
        OUT_OF_SERVICE_AREA_DISTANCE("taxi.out.of.service.area.distance"),
        DRIVER_OFFLINE_INTERVAL("driver.offline.interval"),
        DRIVER_OFFLINE_POLLING_INTERVAL("driver.offline.polling.interval"),
        DRIVER_FIRST_NOTIFIED_COUNT("driver.notified.first.count"),
        DRIVER_BOX_VERSION("driver.box.version"),
        SNAPPMILES_CONVERSION_THRESHOLD("snappmiles.conversion.threshold"),

        // External services
        MYGATE_APPLICATION_ID("mygate.application.id"),
        MYGATE_MERCHANT_ID("mygate.merchant.id"),
        MYGATE_MODE("mygate.mode"),
        SMS_BOX_URL("yoursmsbox.url"),
        SMS_BOX_KEY("yoursmsbox.key"),
        MANDRILL_KEY("mandrill.key"),
        MANDRILL_SEND_URL("mandrill.send.template.url"),
        MAIL_FROM("mail.from"), MAIL_FROM_NAME("mail.from.name"),

        // SMS message content
        SMS_CONTENT_NEW_USER("sms.content.new.user"),
        SMS_CONTENT_PIN_RESET("sms.content.pin.reset"),
        SMS_CONTENT_DRIVER_ARRIVED("sms.content.driver.arrived"),
        SMS_CONTENT_CANCELLED_BY_CUSTOMER("sms.content.cancelled.customer"),
        SMS_CONTENT_CANCELLED_BY_CUSTOMER_FOR_DRIVER("sms.content.cancelled.customer.for.driver"),
        SMS_CONTENT_CANCELLED_BY_DRIVER("sms.content.cancelled.driver"),
        SMS_CONTENT_CANCELLED_BY_ADMIN("sms.content.cancelled.admin"),
        SMS_CONTENT_CANCELLED_BY_ADMIN_FOR_DRIVER("sms.content.cancelled.admin.for.driver"),
        SMS_CONTENT_DRIVER_PAYMENT_SUCCESSFUL("sms.content.payment.successful"),
        SMS_CONTENT_DRIVER_PAYMENT_FAILED("sms.content.payment.failed"),
        SMS_CONTENT_COUPON_REDEEMED("sms.content.coupon.redeemed"),

        // My Message content
        MESSAGE_CONTENT_PASSWORD_RESET("message.content.password.reset"),
        MESSAGE_CONTENT_NEW_USER("message.content.new.user"),
        MESSAGE_CONTENT_NEW_INVOICE("message.content.new.invoice"),
        MESSAGE_CONTENT_PROFILE_UPDATE("message.content.profile.update"),
        MESSAGE_CONTENT_SERVICE_NOT_AVAILABLE("message.content.service.not.available"),
        MESSAGE_CONTENT_CABS_NOT_AVAILABLE("message.content.cabs.not.available");

        Name(final String databaseName) {
            this.databaseName = databaseName;
        }

        private String databaseName;

        public String getDatabaseName() {
            return databaseName;
        }

    }

    public SystemProperty() {
    }

    public SystemProperty(final String name, final String value) {
        super();
        this.name = name;
        this.value = value;
        this.startTime = new Date();
    }

    private String name;
    private String value;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(final Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(final Date endTime) {
        this.endTime = endTime;
    }

}
