package za.co.velvetant.taxi.engine.models;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import static java.lang.String.format;

@Entity
@Audited
public class CaptureTripAmountOverride extends Audit {

    @ManyToOne
    private Fare fare;

    public CaptureTripAmountOverride() {
    }

    public CaptureTripAmountOverride(final Person actionedBy, final Fare fare) {
        super(actionedBy, format("Overriding capture trip [%s] amount by: %s", fare.getReference(), actionedBy.fullName()));
        this.fare = fare;
    }

    public Fare getFare() {
        return fare;
    }
}
