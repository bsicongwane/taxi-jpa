package za.co.velvetant.taxi.engine.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import za.co.velvetant.taxi.engine.models.driver.Driver;

@Entity
@Audited
public class Message extends BaseEntityModel {

    @Column(unique = true, nullable = false)
    private String uuid;

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Customer customer;

    @ManyToOne(optional = true)
    private Driver driver;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    private String subject;

    private String content;

    @Enumerated(EnumType.STRING)
    private MessageTag tag;

    @Enumerated(EnumType.STRING)
    private MessageStatus status;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(final Driver driver) {
        this.driver = driver;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(final Date time) {
        this.time = time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public MessageTag getTag() {
        return tag;
    }

    public void setTag(final MessageTag tag) {
        this.tag = tag;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(final MessageStatus status) {
        this.status = status;
    }

}
