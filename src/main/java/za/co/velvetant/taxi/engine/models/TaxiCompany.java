package za.co.velvetant.taxi.engine.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.envers.Audited;

import za.co.velvetant.taxi.engine.models.driver.Driver;

@Entity
@Audited
public class TaxiCompany extends Company {

    @OneToMany(mappedBy = "taxiCompany")
    private List<Driver> drivers = new ArrayList<>();

    @OneToMany(mappedBy = "taxiCompany")
    private List<AffiliateLocation> locations = new ArrayList<>();

    private String administratorEmail;

    public TaxiCompany() {
    }

    public TaxiCompany(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode, final String registration, final String vat, final Boolean taxExempt) {
        super(companyName, billingAddress1, billingAddress2, billingAddress3, billingPostalCode, postalAddress1, postalAddress2, postalAddress3, postalPostalCode, registration, vat, taxExempt);
    }

    public TaxiCompany(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode, final String contactPerson, final String contactPhone, final String contactMobile,
            final String contactFax, final String contactEmail, final String registration, final String vat, final Boolean taxExempt) {
        super(companyName, billingAddress1, billingAddress2, billingAddress3, billingPostalCode, postalAddress1, postalAddress2, postalAddress3, postalPostalCode, registration, vat, taxExempt);
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(final List<Driver> drivers) {
        this.drivers = drivers;
    }

    public String getAdministratorEmail() {
        return administratorEmail;
    }

    public void setAdministratorEmail(final String administratorEmail) {
        this.administratorEmail = administratorEmail;
    }

    public List<AffiliateLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<AffiliateLocation> locations) {
        this.locations = locations;
    }

}
