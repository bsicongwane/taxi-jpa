package za.co.velvetant.taxi.engine.models.driver;

public enum DriverShiftState {

    AVAILABLE, BUSY, ENDED;
}
