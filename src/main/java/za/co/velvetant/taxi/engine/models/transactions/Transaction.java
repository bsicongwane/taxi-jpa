package za.co.velvetant.taxi.engine.models.transactions;

import static com.google.common.base.Optional.fromNullable;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.TemporalType.TIMESTAMP;
import static za.co.velvetant.taxi.engine.models.transactions.TransactionType.COUPON;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

import za.co.velvetant.taxi.engine.models.BaseEntityModel;
import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Family;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.exceptions.InvalidEntityException;

@Entity
@Audited
public class Transaction extends BaseEntityModel {

    @Column(unique = true, nullable = false)
    @Index(name = "idx_uuid")
    private String uuid = UUID.randomUUID().toString();

    @Temporal(TIMESTAMP)
    private Date date = new Date();

    @Enumerated(STRING)
    private TransactionType type;

    private BigDecimal amount;

    private BigDecimal creditAmount;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Corporate corporate;

    @ManyToOne
    private Family family;

    @ManyToOne
    private Fare trip;

    @ManyToOne
    private CustomerCoupon coupon;

    @SuppressWarnings("UnusedDeclaration")
    public Transaction() {
    }

    public Transaction(final TransactionType type, final Customer customer) {
        this.type = type;
        this.customer = customer;
    }

    public Transaction(final TransactionType type, final Corporate corporate) {
        this.type = type;
        this.corporate = corporate;
    }

    public Transaction(final TransactionType type, final Family family) {
        this.type = type;
        this.family = family;
    }

    public Transaction(final CustomerCoupon coupon) {
        this(COUPON, coupon.getCustomer());
        this.coupon = coupon;
    }

    public Transaction(final TransactionType type, final Fare trip) {
        this.type = type;
        this.trip = trip;

        Customer customer = trip.getCustomer();
        switch (trip.getPaymentMethod()) {
            case CREDIT_CARD: this.customer = customer; break;
            case CORPORATE_ACCOUNT: this.corporate = customer.getCorporate(); break;
            case FAMILY_ACCOUNT: this.family = customer.getFamily(); break;
        }
    }

    public void debit(final Double amount) {
        debit(new BigDecimal(amount));
    }

    public void debit(final BigDecimal amount) {
        BigDecimal debit = new BigDecimal(0);
        if (amount != null) {
            debit = amount.negate();
        }

        this.amount = fromNullable(this.amount).or(new BigDecimal(0.00)).add(debit);
    }

    public void credit(final Double amount) {
        credit(new BigDecimal(amount));
    }

    public void credit(final BigDecimal amount) {
        BigDecimal credit = new BigDecimal(0);
        if (amount != null) {
            credit = amount.abs();
        }

        this.amount = fromNullable(this.amount).or(new BigDecimal(0.00)).add(credit);
        this.creditAmount = amount;
    }

    public String getUuid() {
        return uuid;
    }

    public Date getDate() {
        return date;
    }

    public TransactionType getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Fare getTrip() {
        return trip;
    }

    public void setTrip(final Fare trip) {
        this.trip = trip;
    }

    public CustomerCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(final CustomerCoupon coupon) {
        this.coupon = coupon;
    }

    @PrePersist
    @PreUpdate
    public void validate() {
        if (amount == null) {
            throw new InvalidEntityException("Transaction must have an amount. If zero amount please debit/credit zero amount explicitly.");
        }
    }
}
