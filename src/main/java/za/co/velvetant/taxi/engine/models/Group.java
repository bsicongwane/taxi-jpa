package za.co.velvetant.taxi.engine.models;

public enum Group {

    CUSTOMER,
    DRIVER,
    CORPORATE,
    FAMILY,
    AGENT,
    SUPERVISOR,
    ADMINISTRATOR,
    SYSTEM,
    WEB_USER;
}
