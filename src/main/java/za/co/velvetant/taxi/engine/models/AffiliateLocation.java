package za.co.velvetant.taxi.engine.models;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class AffiliateLocation extends BaseEntityModel {

    private Long radius;

    @Embedded
    private Location location;

    @ManyToOne
    private TaxiCompany taxiCompany;

    protected AffiliateLocation() {
    }

    public AffiliateLocation(final Double latitude, final Double longitude, Long radius) {
        location = new Location(latitude, longitude);
        this.radius = radius;
    }

    public AffiliateLocation(final Double latitude, final Double longitude, final String address, final Long radius) {
        location = new Location(latitude, longitude, address);
        this.radius = radius;
    }

    public Long getRadius() {
        return radius;
    }

    public void setRadius(Long radius) {
        this.radius = radius;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public TaxiCompany getTaxiCompany() {
        return taxiCompany;
    }

    public void setTaxiCompany(TaxiCompany taxiCompany) {
        this.taxiCompany = taxiCompany;
    }

}
