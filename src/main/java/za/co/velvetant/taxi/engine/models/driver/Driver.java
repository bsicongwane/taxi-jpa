package za.co.velvetant.taxi.engine.models.driver;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.envers.Audited;

import za.co.velvetant.taxi.engine.models.DriverStatus;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.Message;
import za.co.velvetant.taxi.engine.models.Person;
import za.co.velvetant.taxi.engine.models.TaxiCompany;

@Entity
@Audited
public class Driver extends Person {

    private String idNumber;

    @OneToMany(mappedBy = "driver")
    private List<Fare> fares;

    @ManyToOne
    private TaxiCompany taxiCompany;

    @Enumerated(EnumType.STRING)
    private DriverStatus status;

    @OneToMany(mappedBy = "driver")
    private List<Message> messages;

    public Driver() {
    }

    public Driver(final String firstName, final String lastName, final String cellphone, final String email, final String idNumber, final String userId, final String password) {
        super(firstName, lastName, cellphone, email, userId, password);
        this.idNumber = idNumber;
        this.status = DriverStatus.OFFLINE;
        setActive(true);
    }

    public Driver(final String firstName, final String lastName, final String cellphone, final String email, final String idNumber, final String userId, final String password,
            final TaxiCompany taxiCompany) {
        this(firstName, lastName, cellphone, email, idNumber, userId, password);
        this.taxiCompany = taxiCompany;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(final String idNumber) {
        this.idNumber = idNumber;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(final List<Fare> fares) {
        this.fares = fares;
    }

    public DriverStatus getStatus() {
        return status;
    }

    public void setStatus(final DriverStatus status) {
        this.status = status;
    }

    public TaxiCompany getTaxiCompany() {
        return taxiCompany;
    }

    public void setTaxiCompany(final TaxiCompany taxiCompany) {
        this.taxiCompany = taxiCompany;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(final List<Message> messages) {
        this.messages = messages;
    }
}
