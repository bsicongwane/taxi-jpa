package za.co.velvetant.taxi.engine.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Family extends BaseEntityModel {

    @Column(unique = true, nullable = false)
    private String uuid;

    private String familyName;

    @OneToMany(mappedBy = "family")
    private List<Customer> members;

    @OneToMany(mappedBy = "headed", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Customer> administrators;

    @OneToMany(mappedBy = "family")
    private List<CreditCard> creditCards = new ArrayList<>();

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(final String familyName) {
        this.familyName = familyName;
    }

    public List<Customer> getMembers() {
        return members;
    }

    public void setMembers(final List<Customer> members) {
        this.members = members;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public List<Customer> getAdministrators() {
        return administrators;
    }

    public void setAdministrators(final List<Customer> administrators) {
        this.administrators = administrators;
    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void addCreditCard(final CreditCard creditCard) {
        creditCards.add(creditCard);
    }

}
