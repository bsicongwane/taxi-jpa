package za.co.velvetant.taxi.engine.models.driver;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import za.co.velvetant.taxi.engine.models.BaseEntityModel;
import za.co.velvetant.taxi.engine.models.DriverStatus;
import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.engine.models.TaxiStatus;

@Entity
@NamedQueries({
        @NamedQuery(name = DriverShift.Queries.FIND_DRIVER_BY_REGISTRATION, query = "SELECT ds.driver FROM DriverShift ds WHERE ds.taxi.registration = ?1 AND ds.state = za.co.velvetant.taxi.engine.models.driver.DriverShiftState.AVAILABLE"),
        @NamedQuery(name = DriverShift.Queries.FIND_ACTIVE_DRIVER_SHIFT_WITH_DRIVER_OR_TAXI, query = "select ds from DriverShift ds where (ds.driver = ?1 or ds.taxi = ?2) and state != za.co.velvetant.taxi.engine.models.driver.DriverShiftState.ENDED"),
        @NamedQuery(name = DriverShift.Queries.FIND_ACTIVE_DRIVER_SHIFT_WITH_DRIVER_USERID, query = "select ds from DriverShift ds where ds.driver.userId = ?1 and state != za.co.velvetant.taxi.engine.models.driver.DriverShiftState.ENDED")})
public class DriverShift extends BaseEntityModel {

    public static final class Queries {

        public static final String FIND_DRIVER_BY_REGISTRATION = "DriverShift.findDriverByRegistration";
        public static final String FIND_ACTIVE_DRIVER_SHIFT_WITH_DRIVER_OR_TAXI = "DriverShift.findActiveDriverShiftDriverOrTaxi";
        public static final String FIND_ACTIVE_DRIVER_SHIFT_WITH_DRIVER_USERID = "DriverShift.findActiveDriverShiftWithDriverUserId";
    }

    @Column(length = 36, unique = true)
    private String shiftIdentifier;

    @Enumerated(EnumType.STRING)
    private DriverShiftState state;

    private Date started;
    private Date ended;
    private Boolean system;

    @ManyToOne(optional = false)
    private Taxi taxi;

    @ManyToOne(optional = false)
    private Driver driver;

    public DriverShift() {
    }

    public DriverShift(final Driver driver, final Taxi taxi) {
        if (driver.getActive() == null || !driver.getActive()) {
            throw new RuntimeException("Driver is not active");
        }

        if (driver.getActive() == null || !taxi.getActive()) {
            throw new RuntimeException("Taxi is not active");
        }

        this.driver = driver;
        this.taxi = taxi;
        shiftIdentifier = UUID.randomUUID().toString();

        startShift();
    }

    private void startShift() {

        state = DriverShiftState.AVAILABLE;
        started = new Date();

        available();
    }

    public String getShiftIdentifier() {

        return shiftIdentifier;
    }

    public Taxi getTaxi() {

        return taxi;
    }

    public String getDriverId() {

        return driver.getUserId();
    }

    public String getDriverFirstName() {

        return driver.getFirstName();
    }

    public String getDriverLastName() {

        return driver.getLastName();
    }

    public String getTaxiRegistration() {

        return taxi.getRegistration();
    }

    public Driver getDriver() {

        return driver;
    }

    public Date getStarted() {
        return started;
    }

    public Date getEnded() {
        return ended;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(final Boolean system) {
        this.system = system;
    }

    public boolean isAvailable() {

        return state == DriverShiftState.AVAILABLE;
    }

    public boolean hasEnded() {

        return DriverShiftState.ENDED.equals(state);
    }

    public DriverShiftState getState() {
        return state;
    }

    public void available() {

        if (!DriverShiftState.ENDED.equals(state)) {

            state = DriverShiftState.AVAILABLE;
            driver.setStatus(DriverStatus.ONLINE);
            taxi.setStatus(TaxiStatus.ONLINE);
        }
    }

    public void busy() {

        if (!DriverShiftState.ENDED.equals(state)) {

            state = DriverShiftState.BUSY;

            driver.setStatus(DriverStatus.BUSY);
            taxi.setStatus(TaxiStatus.BUSY);
        }
    }

    public void endShift() {

        ended = new Date();
        state = DriverShiftState.ENDED;

        driver.setStatus(DriverStatus.OFFLINE);
        taxi.setStatus(TaxiStatus.OFFLINE);
    }


    public void endShift(final Boolean system) {
        endShift();
        this.system = system;
    }

    @Override
    public int hashCode() {

        return new HashCodeBuilder().append(shiftIdentifier).toHashCode();
    }

    @Override
    public boolean equals(final Object o) {

        if (!(o instanceof DriverShift)) {

            return false;
        }

        return new EqualsBuilder().append(shiftIdentifier, ((DriverShift) o).shiftIdentifier).isEquals();
    }
}
