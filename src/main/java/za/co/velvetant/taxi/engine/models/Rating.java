package za.co.velvetant.taxi.engine.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
public class Rating extends BaseEntityModel {

    @Enumerated(EnumType.STRING)
    private RatingLevel tripRating;

    @Enumerated(EnumType.STRING)
    private RatingLevel driverRating;

    @OneToOne(optional = false)
    private Fare fare;

    private String feedback;

    public Rating() {
    }

    public Rating(final RatingLevel tripRating, final RatingLevel driverRating) {
        this.tripRating = tripRating;
        this.driverRating = driverRating;
    }

    public RatingLevel getTripRating() {
        return tripRating;
    }

    public void setTripRating(final RatingLevel tripRating) {
        this.tripRating = tripRating;
    }

    public RatingLevel getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(final RatingLevel driverRating) {
        this.driverRating = driverRating;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(final Fare fare) {
        this.fare = fare;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(final String feedback) {
        this.feedback = feedback;
    }

}
